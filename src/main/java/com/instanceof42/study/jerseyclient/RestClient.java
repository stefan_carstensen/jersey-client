package com.instanceof42.study.jerseyclient;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

public class RestClient {

    private static final String REST_URI = "http://localhost:8080/book";

    private Client client = ClientBuilder.newClient();

    public Book getBook(int id) {
        return client
                .target(REST_URI)
                .path(String.valueOf(id))
                .request(MediaType.APPLICATION_JSON)
                .get(Book.class);
    }

}
