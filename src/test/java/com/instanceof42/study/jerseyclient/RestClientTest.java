package com.instanceof42.study.jerseyclient;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.Test;

import javax.ws.rs.core.MediaType;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.ResponseProcessingException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

public class RestClientTest {

    private static final Integer HTTP_PORT = 8080;
    private static final Integer HTTPS_PORT = 8443;

    @Rule
    public final WireMockRule serviceMock = new WireMockRule(wireMockConfig()
            .httpsPort(HTTPS_PORT)
            .port(HTTP_PORT));

    @Test
    public void getBook() {

        serviceMock.stubFor(get(urlPathEqualTo("/book/23"))
                .willReturn(
                        aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                                .withBody("{\"id\":\"23\", \"name\":\"A wonderful book\"}")));

        RestClient client = new RestClient();

        Book book = client.getBook(23);
        assertNotNull(book);

    }

    @Test
    public void getBookNotFound() {

        serviceMock.stubFor(get(urlPathEqualTo("/book/23"))
                .willReturn(
                        aResponse()
                                .withStatus(404)));

        RestClient client = new RestClient();

        Book book = null;
        try {
            book = client.getBook(23);
            fail("NotFoundException expected.");
        } catch (NotFoundException e) {

        }
        assertNull(book);

    }
    
    @Test
    public void getBookNotAllowed() {

        serviceMock.stubFor(get(urlPathEqualTo("/book/23"))
                .willReturn(
                        aResponse()
                                .withStatus(403)));

        RestClient client = new RestClient();

        Book book = null;
        try {
            book = client.getBook(23);
            fail("ForbiddenException expected.");
        } catch (ClientErrorException e) {
            assertEquals(403, e.getResponse().getStatus());
        }
        assertNull(book);

    }    
    
   @Test
    public void getBookWrongAnswer() {

        serviceMock.stubFor(get(urlPathEqualTo("/book/23"))
                .willReturn(
                        aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                                .withBody("just a string")));

        RestClient client = new RestClient();

        Book book =  null;
        try {
            book = client.getBook(23);
            fail("ResponseProcessingException expected.");
        }
        catch (ResponseProcessingException e) {
        }
        assertNull(book);

    }
    
}
